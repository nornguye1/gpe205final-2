﻿//Norman Nguyen
//Tank Camera: A camera that follows the tank as it moves and rotates.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankCamera : MonoBehaviour
{
    public Transform player;
    //Offset the vector to allow the camera to not tie to the tank
    public Vector3 offset;
    //Turn Speed for the camera
    public float turnSpeed;
    public GameObject playerPrefab;
    //Moves the tank while updating on the position for the camera.
    void Update()
    {
        player = GameObject.Find("Tank").transform;
        //Camera moves as it follows the cannon
        //Updating on the position of the character and camera
        transform.position = player.position + offset;
        //Positioning to look at the player
        transform.LookAt(player.transform);
        transform.position = player.transform.position + offset;
        //A Key
        if (Input.GetKey(KeyCode.A))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //D Key
        if (Input.GetKey(KeyCode.D))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //Q Key camera (THIS IS WHEN IF THE CAMERA IS NOT ORIENTATED)

        if (Input.GetKey(KeyCode.Q))
        {
            transform.RotateAround(player.transform.position, Vector3.up, -50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
        //E Key camera (THIS IS WHEN IF THE CAMERA IS NOT ORIENTATED)

        if (Input.GetKey(KeyCode.E))
        {
            transform.RotateAround(player.transform.position, Vector3.up, 50 * Time.deltaTime);
            offset = transform.position - player.transform.position;
        }
    }
}

