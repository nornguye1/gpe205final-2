﻿//Norman Nguyen
//Score Manager: Manage score contents
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    //Text
    public Text scoreText;
    public Text highScoreText;
    public TankData data;
    //High Score
    public int highScore;
	// Use this for initialization
	void Start ()
	{
	    scoreText = GetComponent<Text>();
	    data = GetComponent<TankData>();
        //Find Text
        scoreText = GameObject.Find("P1Score").GetComponent<Text>();
        //Find High Score Text
	    highScoreText = GameObject.Find("HighScoreText").GetComponent<Text>();
        highScoreText.text = "High Score: " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        setUIScore();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    setUIScore();
    }
    //UI Score and High Score update
    public void setUIScore()
    {
        scoreText.text = "Score: " + data.currentScore.ToString();
        //Set your high score
        if (data.currentScore > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", data.currentScore);
            highScoreText.text = "High Score: " + data.currentScore.ToString();
        }
    }
}
