﻿//Norman Nguyen
//Score Manager 2: Score Manager for the Player 2
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager2 : MonoBehaviour
{
    //Text
    public Text scoreText;
    public Text highScoreText2;
    public TankData data;
    // Use this for initialization
    void Start()
    {
        scoreText = GetComponent<Text>();
        data = GetComponent<TankData>();
        scoreText = GameObject.Find("P2Score").GetComponent<Text>();
        highScoreText2 = GameObject.Find("HighScoreText2").GetComponent<Text>();
        highScoreText2.text = "High Score: " + PlayerPrefs.GetInt("HighScore2", 0).ToString();
        setUIScore();
    }

    // Update is called once per frame
    void Update()
    {
        setUIScore();
    }
    //UI Score and High Score update
    public void setUIScore()
    {
        scoreText.text = "Score: " + data.currentScore.ToString();
        //Set your high score
        if (data.currentScore > PlayerPrefs.GetInt("HighScore2", 0))
        {
            PlayerPrefs.SetInt("HighScore2", data.currentScore);
            highScoreText2.text = "High Score: " + data.currentScore.ToString();
        }
    }
}
