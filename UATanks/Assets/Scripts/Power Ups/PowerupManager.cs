﻿//Norman Nguyen
//Powerup Manager, this is for your tank and AI to have the powerup within your tanks.
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerupManager : MonoBehaviour
{
    //Powerup List
    public List<Powerup> powerups;

    //TankData
    private TankData data;

    // Use this for initialization
    void Start()
    {
        //Get Component
        data = GetComponent<TankData>();
    }

    // Update is called once per frame
    void Update()
    {
        //Create local list of powerups to remove
        List<Powerup> powerupsToRemove = new List<Powerup>();
        //Go through all of the powerups
        foreach (Powerup powerup in powerups)
        {
            //Countdown subtract based on delta time.
            powerup.countdown -= Time.deltaTime;
            //If it hits to 0, it removes the powerup
            if (powerup.countdown <= 0)
            {
                powerupsToRemove.Add(powerup);
            }
        }
        //Remove the item with a foreach method
        foreach (Powerup powerup in powerupsToRemove)
        {
            RemovePowerup(powerup);
        }
    }
    //Add the powerup
    public void AddPowerup(Powerup powerupToAdd)
    {
        //Debug Log to show on console
        Debug.Log("Adding Powerup!");
        Powerup newPowerup = new Powerup();
        //Add Poweruphealth
        if (powerupToAdd.powerupType == Powerup.PowerupType.HealthPowerup)
        {
            newPowerup = new PowerupHealth((PowerupHealth)powerupToAdd);
        }
        //Add PowerupSpeed
        else if (powerupToAdd.powerupType == Powerup.PowerupType.SpeedPowerup)
        {
            newPowerup = new PowerupSpeed((PowerupSpeed)powerupToAdd);
        }
        //Add PowerupRapidFire
        else if (powerupToAdd.powerupType == Powerup.PowerupType.RapidFirePowerup)
        {
            newPowerup = new PowerupRapidFire((PowerupRapidFire)powerupToAdd);
        }
        // Add to the list
        powerups.Add(newPowerup);
        // Adjust the tankdata with the powerup
        newPowerup.OnAddPowerup(data);
    }
    //Remove the powerup
    public void RemovePowerup(Powerup powerupToRemove)
    {
        Debug.Log("Removing Powerup!");
        //Remove Powerup
        powerups.Remove(powerupToRemove);
        //Adjusting the tankdata in the player and AI
        powerupToRemove.OnRemovePowerup(data);
    }
}